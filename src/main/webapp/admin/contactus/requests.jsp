<%--
  Created by IntelliJ IDEA.
  User: sudhanshu
  Date: 04/03/21
  Time: 2:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.util.*"%>
<%@ page import="model.Request" %>
<html>
<head>
    <title>Request</title>
    <link rel="stylesheet" href="../stylesheet.css">
</head>
<body >
  <%
      if(session.getAttribute("username")!=null){
  %>
<div class="request-center">
    <div class="logout-div">
        <form action="LogoutServlet" method="post">
            <input id="logout" type="submit" name="logout" value="Logout">
        </form>
    </div>
    <div class="active-archive-div">
        <%
            List<Request> activeList = (ArrayList<Request>)request.getAttribute("activeList");
            if(activeList.size()!=0){
        %>
        <h3>Active Table</h3>
        <div class="table-div">
            <table class="table">
                <thead >
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Created-At</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody class="bg-danger">
                <%
                    for(Request activeRequest : activeList) {
                %>
                <tr>
                    <td id="tdname"><%= activeRequest.getName()  %></td>
                    <td class="tdemail"><%= activeRequest.getEmail() %></td>
                    <td><%= activeRequest.getMessage() %></td>
                    <td><%= activeRequest.getTimestamp() %></td>
                    <td>
                        <form method="post" action="requests">
                            <input type="hidden" name="contactUsId" value="<%= activeRequest.getContactUsId() %>" >
                            <input type="hidden" name="currentStatus" value="<%= activeRequest.getActive() %>" >
                            <input type="submit" name="archive" value="Archive"  >
                        </form>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>
        <%
            }
        %>

        <%
            List<Request> archiveList = (ArrayList<Request>)request.getAttribute("archiveList");
            if(archiveList.size()!=0){
        %>
        <h3>Archive Table</h3>
        <div class="table-div">
            <table class="table ">
                <thead >
                <tr class="bg-warning">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Created-At</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody class="bg-danger">
                <%
                    for(Request archiveRequest : archiveList) {
                %>
                <tr>
                    <td><%= archiveRequest.getName()  %></td>
                    <td class="tdemail"><%= archiveRequest.getEmail() %></td>
                    <td><%= archiveRequest.getMessage() %></td>
                    <td><%= archiveRequest.getTimestamp() %></td>
                    <td>
                        <form method="post" action="requests">
                            <input type="hidden" name="contactUsId" value="<%= archiveRequest.getContactUsId() %>" >
                            <input type="hidden" name="currentStatus" value="<%= archiveRequest.getActive() %>" >
                            <input type="submit" name="archive" value="Active"  >
                        </form>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>
        <%
            }
        %>
    </div>
</div>
  <%
      }
      else{
          response.sendRedirect("index.jsp");
      }
  %>
</body>
</html>
