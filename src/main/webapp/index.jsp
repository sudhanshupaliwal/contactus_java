<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Contact Us</title>
    <link rel="stylesheet" href="stylesheet.css">
</head>
<body class="index-body">
<div class="center">
    <div class="contact-us-form" >
        <h3>Contact US Form </h3>
        <form action="ContactUsFormServlet" method="post">
            <label for="name">Full Name</label> <br>
            <input type="text" id="name" name="name" required> <br>
            <label for="email">Email</label> <br>
            <input type="email" id="email" name="email" required> <br>
            <label for="message">Message</label> <br>
            <textarea name="message" id="message" cols="30" rows="10"></textarea> <br> <br>
            <input type="submit" id="button" name="button" value="Submit" >
        </form>
        <br>
        <br>
       <div id="admin-login-button">
           <a  id="admin-text" href="admin/login.jsp">Admin Login</a>
       </div>
    </div>
</div>
<br>
<br>
</body>
</html>