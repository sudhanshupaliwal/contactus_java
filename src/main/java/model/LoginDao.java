package model;
import java.sql.*;

public class LoginDao {
    public boolean validateLogin(User user){
        boolean status = false;
        try{
            Class.forName("org.postgresql.Driver");
            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/demo",
                    "postgres", "root1234");
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM public.admindetails WHERE email=? AND password=? ");
            stmt.setString(1,user.getUsername());
            stmt.setString(2, user.getPassword());
            ResultSet rs = stmt.executeQuery();
            status = rs.next();
        }
        catch(Exception e){
            e.printStackTrace();
        }
      return status;
    }
}
