package model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class RequestDao {
    public void saveContactUsRequest(Request request){
        try{
            Class.forName("org.postgresql.Driver");
            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/demo",
                    "postgres", "root1234");
            PreparedStatement stmt = con.prepareStatement("INSERT INTO public.contactrequest(name,email,message) VALUES(?,?,?)");
            stmt.setString(1,request.getName());
            stmt.setString(2,request.getEmail());
            stmt.setString(3, request.getMessage());
            stmt.executeUpdate();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    public List<Request> fetchRequests(boolean activeRequest){
        List<Request> requests = new ArrayList<>();
        try{
            Class.forName("org.postgresql.Driver");
            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/demo",
                    "postgres", "root1234");
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM public.contactrequest WHERE active=? ORDER BY created_at DESC ");
            stmt.setBoolean(1 , activeRequest);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                Request request = new Request();
                request.setContactUsId(rs.getInt(1));
                request.setName(rs.getString(2));
                request.setEmail(rs.getString(3));
                request.setMessage(rs.getString(4));
                request.setTimestamp(rs.getString(5));
                request.setActive(rs.getBoolean(6));
                requests.add(request);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return requests;
    }
}
