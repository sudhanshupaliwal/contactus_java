package controllers;
import model.Request;
import model.RequestDao;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.*;

@WebServlet(name = "requests", value = "/admin/requests")
public class RequestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         HttpSession session = request.getSession();
         if(session.getAttribute("username")!=null){
             RequestDao requestDao = new RequestDao();
             request.setAttribute("activeList" , requestDao.fetchRequests(true));
             request.setAttribute("archiveList" , requestDao.fetchRequests(false));
             RequestDispatcher rd = request.getRequestDispatcher("contactus/requests.jsp");
             rd.forward(request , response);
         }
         else{
             response.sendRedirect("login.jsp");
         }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("username")!=null){
            try{
                Class.forName("org.postgresql.Driver");
                Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/demo",
                        "postgres", "root1234");
                PreparedStatement stmt = con.prepareStatement("UPDATE public.contactrequest SET active=?  WHERE id=? ");
                stmt.setBoolean(1, !Boolean.parseBoolean(request.getParameter("currentStatus")));
                stmt.setInt(2,Integer.parseInt(request.getParameter("contactUsId")));
                stmt.executeUpdate();
            }
            catch(Exception e){
                e.printStackTrace();
            }
            response.sendRedirect("requests");
        }
        else{
            response.sendRedirect("login.jsp");
        }
    }
}
