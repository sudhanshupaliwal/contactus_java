package controllers;
import model.User;
import model.LoginDao;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "authenticator", value = "/admin/authenticator")
public class AuthenticatorServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        user.setUsername(request.getParameter("username"));
        user.setPassword(request.getParameter("password"));
        LoginDao logindao = new LoginDao();
        boolean isAdmin =  logindao.validateLogin(user);
        if(isAdmin) {
            HttpSession session = request.getSession();
            session.setAttribute("username", user.getUsername());
            response.sendRedirect("requests");
        }
        else{
            response.sendRedirect("login.jsp");
        }
    }
}
