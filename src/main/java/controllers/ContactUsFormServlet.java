package controllers;
import model.Request;
import model.RequestDao;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ContactUsFormServlet", value = "/ContactUsFormServlet")
public class ContactUsFormServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest requestServlet, HttpServletResponse response) throws ServletException, IOException {
        Request request = new Request();
        request.setName(requestServlet.getParameter("name"));
        request.setEmail(requestServlet.getParameter("email"));
        request.setMessage(requestServlet.getParameter("message"));
        RequestDao contactUsDao = new RequestDao();
        contactUsDao.saveContactUsRequest(request);
        response.sendRedirect("index.jsp");
    }
}
