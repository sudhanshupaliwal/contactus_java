<%--
  Created by IntelliJ IDEA.
  User: sudhanshu
  Date: 03/03/21
  Time: 11:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="../stylesheet.css">
</head>
<body class="request-body">
<div class="center">
    <div class="login-div">
        <h3>Login</h3> <br>
        <form action="authenticator" method="post">
            <label for="username">Enter UserName</label> <br>
            <input type="text" id="username" name="username" required> <br>
            <label for="password">Enter Password</label> <br>
            <input type="password" id="password" name="password" required> <br>
            <input type="submit" id="login-button" value="Login"> <br>
        </form>
    </div>
</div>
</body>
</html>
